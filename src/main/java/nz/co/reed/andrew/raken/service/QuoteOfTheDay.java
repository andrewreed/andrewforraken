package nz.co.reed.andrew.raken.service;

public interface QuoteOfTheDay {

    public String getQuoteOfTheDay();
}
