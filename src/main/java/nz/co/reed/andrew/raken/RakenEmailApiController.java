package nz.co.reed.andrew.raken;

import lombok.extern.slf4j.Slf4j;
import nz.co.reed.andrew.raken.model.RakenEmail;
import nz.co.reed.andrew.raken.model.RakenValidationException;
import nz.co.reed.andrew.raken.service.SendMailToSendGrid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/rakenapi")
@Slf4j
public class RakenEmailApiController {

    @Autowired
    private SendMailToSendGrid sendMailToSendGrid;

    @PostMapping(value="/send")
    public void sendEmail(@RequestBody RakenEmail emailToSend, @RequestParam(required=false) boolean enrich) {
        log.debug("Email received." + emailToSend.toString());

        //validate the parts are there.
        if (emailToSend.getTo() == null || emailToSend.getTo().isEmpty()) {
            throw new RakenValidationException("To is not provided");
        }
        if (emailToSend.getBody() == null || emailToSend.getBody().isEmpty()) {
            throw new RakenValidationException("Body is not provided");
        }
        if (emailToSend.getSubject() == null || emailToSend.getSubject().isEmpty()) {
            throw new RakenValidationException("Body is not provided");
        }

        sendMailToSendGrid.sendMailToSendGrid(emailToSend, enrich);

    }
}
