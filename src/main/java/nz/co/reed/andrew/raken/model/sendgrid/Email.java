
package nz.co.reed.andrew.raken.model.sendgrid;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "personalizations",
    "content",
    "from",
    "reply_to"
})
public class Email {

    @JsonProperty("personalizations")
    private List<nz.co.reed.andrew.raken.model.sendgrid.Personalization> personalizations = null;
    @JsonProperty("content")
    private List<Content> content = null;
    @JsonProperty("from")
    private nz.co.reed.andrew.raken.model.sendgrid.From from;
    @JsonProperty("reply_to")
    private nz.co.reed.andrew.raken.model.sendgrid.ReplyTo replyTo;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("personalizations")
    public List<nz.co.reed.andrew.raken.model.sendgrid.Personalization> getPersonalizations() {
        return personalizations;
    }

    @JsonProperty("personalizations")
    public void setPersonalizations(List<nz.co.reed.andrew.raken.model.sendgrid.Personalization> personalizations) {
        this.personalizations = personalizations;
    }

    @JsonProperty("content")
    public List<Content> getContent() {
        return content;
    }

    @JsonProperty("content")
    public void setContent(List<Content> content) {
        this.content = content;
    }

    @JsonProperty("from")
    public nz.co.reed.andrew.raken.model.sendgrid.From getFrom() {
        return from;
    }

    @JsonProperty("from")
    public void setFrom(nz.co.reed.andrew.raken.model.sendgrid.From from) {
        this.from = from;
    }

    @JsonProperty("reply_to")
    public nz.co.reed.andrew.raken.model.sendgrid.ReplyTo getReplyTo() {
        return replyTo;
    }

    @JsonProperty("reply_to")
    public void setReplyTo(nz.co.reed.andrew.raken.model.sendgrid.ReplyTo replyTo) {
        this.replyTo = replyTo;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
