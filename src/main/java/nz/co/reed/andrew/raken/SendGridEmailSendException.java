package nz.co.reed.andrew.raken;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class SendGridEmailSendException extends RuntimeException {
    String message;
    HttpStatus status;

    public SendGridEmailSendException(String message, HttpStatus status) {

        this.message = message;
        this.status = status;
    }

    public String getSendGridsErrorMessage() {
        return "SendGrid gave us this error: HttpStatus =" + status + ": Message was = " + message;
    }
}
