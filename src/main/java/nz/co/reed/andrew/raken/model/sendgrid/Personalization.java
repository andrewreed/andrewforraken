
package nz.co.reed.andrew.raken.model.sendgrid;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import nz.co.reed.andrew.raken.model.sendgrid.Bcc;
import nz.co.reed.andrew.raken.model.sendgrid.Cc;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "to",
    "cc",
    "bcc",
    "subject"
})
public class Personalization {

    @JsonProperty("to")
    private List<nz.co.reed.andrew.raken.model.sendgrid.To> to = null;
    @JsonProperty("cc")
    private List<Cc> cc = null;
    @JsonProperty("bcc")
    private List<Bcc> bcc = null;
    @JsonProperty("subject")
    private String subject;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("to")
    public List<nz.co.reed.andrew.raken.model.sendgrid.To> getTo() {
        return to;
    }

    @JsonProperty("to")
    public void setTo(List<nz.co.reed.andrew.raken.model.sendgrid.To> to) {
        this.to = to;
    }

    @JsonProperty("cc")
    public List<Cc> getCc() {
        return cc;
    }

    @JsonProperty("cc")
    public void setCc(List<Cc> cc) {
        this.cc = cc;
    }

    @JsonProperty("bcc")
    public List<Bcc> getBcc() {
        return bcc;
    }

    @JsonProperty("bcc")
    public void setBcc(List<Bcc> bcc) {
        this.bcc = bcc;
    }

    @JsonProperty("subject")
    public String getSubject() {
        return subject;
    }

    @JsonProperty("subject")
    public void setSubject(String subject) {
        this.subject = subject;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
