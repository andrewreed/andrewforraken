package nz.co.reed.andrew.raken.service;

import lombok.extern.slf4j.Slf4j;
import nz.co.reed.andrew.raken.model.quoteoftheday.QuoteOfTheDayResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class QuoteOfTheDayService implements QuoteOfTheDay {
    @Override
    public String getQuoteOfTheDay() {

        RestTemplate quoteService = new RestTemplate();
        ResponseEntity<QuoteOfTheDayResponse> response = quoteService.getForEntity("https://quotes.rest/qod?language=en", QuoteOfTheDayResponse.class);

                //get the quote and Author
        StringBuilder quoteAndAuthor = new StringBuilder();
        quoteAndAuthor.append(response.getBody().getContents().getQuotes().get(0).getQuote())
                      .append("\n  -- ")
                      .append(response.getBody().getContents().getQuotes().get(0).getAuthor());

        return quoteAndAuthor.toString();
    }
}
