package nz.co.reed.andrew.raken.model;

public class RakenValidationException extends RuntimeException {

    public RakenValidationException(String message) {
        super(message);
    }
}
