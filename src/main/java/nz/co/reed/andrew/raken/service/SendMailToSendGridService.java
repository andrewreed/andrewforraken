package nz.co.reed.andrew.raken.service;

import java.util.ArrayList;
import java.util.Arrays;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.tools.javadoc.Start;
import lombok.extern.slf4j.Slf4j;
import nz.co.reed.andrew.raken.SendGridEmailSendException;
import nz.co.reed.andrew.raken.model.RakenEmail;
import nz.co.reed.andrew.raken.model.RakenFilterDomainException;
import nz.co.reed.andrew.raken.model.sendgrid.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class SendMailToSendGridService implements SendMailToSendGrid {

    @Value("${accept.rakenapp.com.domain.only:false}")
    private boolean filterRakenDomain;

    @Value("${sendgrid_api_host}")
    private String sendGridApi;

    @Value("${sendgrid_api_key}")
    private String sendGridKey;

    @Autowired
    private QuoteOfTheDay quoteOfTheDay;

    @Override
    public void sendMailToSendGrid(RakenEmail email, boolean enrich)  {
        if (filterRakenDomain && !email.getTo().toLowerCase().contains("rakenapp.com")) {
            throw new RakenFilterDomainException("Filtered out non Raken domain", email);
        }

        //Get the Quote of the Day, if we're enriching the email.
        if (enrich) {
            String quoteAndAuthor = quoteOfTheDay.getQuoteOfTheDay();
            email.setBody(email.getBody() + "\n\n" + quoteAndAuthor);
        }

        Email sendGridEmail = new Email();

        //start with required fields
        Content emailContent = new Content();
        emailContent.setValue(email.getBody());
        emailContent.setType(MediaType.TEXT_PLAIN_VALUE);
        sendGridEmail.setContent(Arrays.asList(emailContent));

        Personalization personalization = new Personalization();
        personalization.setSubject(email.getSubject());
        To to = new To();
        to.setEmail(email.getTo());
        personalization.setTo(Arrays.asList(to));

        if (email.getCc() != null && !email.getCc().isEmpty()) {
            Cc cc = new Cc();
            cc.setEmail(email.getCc());
            personalization.setCc(Arrays.asList(cc));
        }

        if (email.getBcc() != null && !email.getBcc().isEmpty()) {
            Bcc bcc = new Bcc();
            bcc.setEmail(email.getBcc());
            personalization.setBcc(Arrays.asList(bcc));
        }
        sendGridEmail.setPersonalizations(Arrays.asList(personalization));

        From from = new From();
        from.setEmail("andrew.b.reed@gmail.com");
        sendGridEmail.setFrom(from);






        //send to Send Grid
        RestTemplate sendGridRestTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(sendGridKey);
        headers.setContentType(MediaType.APPLICATION_JSON);

        ObjectMapper mapper = new ObjectMapper();
        String json = "";
        try {
            json = mapper.writer().withDefaultPrettyPrinter().writeValueAsString(sendGridEmail);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        HttpEntity<String> request = new HttpEntity<>(json, headers);


        ResponseEntity<String> response = sendGridRestTemplate.postForEntity(sendGridApi, request, String.class);
        if (!response.getStatusCode().is2xxSuccessful()) {
            throw new SendGridEmailSendException(response.getBody(), response.getStatusCode());
        }

    }
}
