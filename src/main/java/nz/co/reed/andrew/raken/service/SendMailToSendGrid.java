package nz.co.reed.andrew.raken.service;

import nz.co.reed.andrew.raken.model.RakenEmail;

public interface SendMailToSendGrid {


    public void sendMailToSendGrid(RakenEmail email, boolean enrich);
}
