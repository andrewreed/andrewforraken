package nz.co.reed.andrew.raken;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RakenApplication {

	public static void main(String[] args) {
		SpringApplication.run(RakenApplication.class, args);
	}

}
