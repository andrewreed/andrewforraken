package nz.co.reed.andrew.raken;

import lombok.extern.slf4j.Slf4j;
import nz.co.reed.andrew.raken.model.RakenFilterDomainException;
import nz.co.reed.andrew.raken.model.RakenValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class RakenEmailSendControllerAdvice {

    @ExceptionHandler({RakenFilterDomainException.class})
    public ResponseEntity<String> handleRakenFilterDomainException(RakenFilterDomainException e) {
        log.error("Filter caught a non Raken Domain. Email Message is:" +e.getEmail().toString());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }

    @ExceptionHandler({RakenValidationException.class})
    public ResponseEntity<String> handleRakenValidationException(RakenValidationException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }

    @ExceptionHandler({SendGridEmailSendException.class})
    public ResponseEntity<String> handleRakenValidationException(SendGridEmailSendException e) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getSendGridsErrorMessage());
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<String> handleException(Exception e) {

        log.error("Error encountered", e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
    }
}
