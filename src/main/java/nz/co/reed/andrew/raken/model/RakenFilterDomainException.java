package nz.co.reed.andrew.raken.model;

import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
@Data
public class RakenFilterDomainException extends RuntimeException {

    private RakenEmail email;

    public RakenFilterDomainException(String message, RakenEmail email) {
        super(message);
        this.email = email;

    }
}
