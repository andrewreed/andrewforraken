package nz.co.reed.andrew.raken;

import com.fasterxml.jackson.databind.ObjectMapper;
import nz.co.reed.andrew.raken.model.RakenEmail;
import nz.co.reed.andrew.raken.service.SendMailToSendGrid;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureJsonTesters
class RakenApplicationControllerTests {

	@MockBean
	private SendMailToSendGrid sendMailToSendGrid;

	@Autowired
	private MockMvc mockmvc;

	@Test
	public void testNoToFieldError() throws Exception {
		RakenEmail emailToSend = new RakenEmail();
		emailToSend.setBody("Fake body");
		emailToSend.setSubject("Fake Subject");

		Mockito.doNothing().when(this.sendMailToSendGrid).sendMailToSendGrid(emailToSend, false);

		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writer().withDefaultPrettyPrinter().writeValueAsString(emailToSend);

		MvcResult res = this.mockmvc
				.perform(post("/v1/rakenapi/send").contentType(MediaType.APPLICATION_JSON).content(json))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().is4xxClientError())
				.andExpect(content().string("To is not provided"))
				.andReturn();
	}

    @Test
    public void testCompleteRequestNoEnrich() throws Exception {
        RakenEmail emailToSend = new RakenEmail();
        emailToSend.setBody("Fake body");
        emailToSend.setSubject("Fake Subject");
        emailToSend.setTo("me@mydomain.com");
        emailToSend.setCc("cc@mydomain.com");
        emailToSend.setBody("bcc@mydomain.com");

        Mockito.doNothing().when(this.sendMailToSendGrid).sendMailToSendGrid(emailToSend, false);

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writer().withDefaultPrettyPrinter().writeValueAsString(emailToSend);

        MvcResult res = this.mockmvc
                .perform(post("/v1/rakenapi/send").contentType(MediaType.APPLICATION_JSON).content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().is2xxSuccessful())
                .andReturn();
    }

	@Test
	public void testCompleteRequestEnriched() throws Exception {
		RakenEmail emailToSend = new RakenEmail();
		emailToSend.setBody("Fake body");
		emailToSend.setSubject("Fake Subject");
		emailToSend.setTo("me@mydomain.com");
		emailToSend.setCc("cc@mydomain.com");
		emailToSend.setBody("bcc@mydomain.com");

		Mockito.doNothing().when(this.sendMailToSendGrid).sendMailToSendGrid(emailToSend, true);

		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writer().withDefaultPrettyPrinter().writeValueAsString(emailToSend);

		MvcResult res = this.mockmvc
				.perform(post("/v1/rakenapi/send?enrich=true").contentType(MediaType.APPLICATION_JSON).content(json))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(status().is2xxSuccessful())
				.andReturn();
	}

}
