package nz.co.reed.andrew.raken;

import nz.co.reed.andrew.raken.service.QuoteOfTheDay;
import nz.co.reed.andrew.raken.service.QuoteOfTheDayService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;

import static org.junit.jupiter.api.Assertions.assertEquals;


@AutoConfigureJsonTesters
public class QuoteOfTheDayTests {


    @Test
    public void testLiveQuoteOfTheDayService() throws Exception {

        QuoteOfTheDay quoteOfTheDay = new QuoteOfTheDayService();
        String quote = quoteOfTheDay.getQuoteOfTheDay();
        assertEquals(!quote.isEmpty(), "Didn't get a quote of the day");
    }
}
