package nz.co.reed.andrew.raken;

import nz.co.reed.andrew.raken.model.RakenEmail;
import nz.co.reed.andrew.raken.service.SendMailToSendGrid;
import nz.co.reed.andrew.raken.service.SendMailToSendGridService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;

import static org.junit.jupiter.api.Assertions.assertEquals;

@AutoConfigureJsonTesters
public class SendMailViaSendGridTests {

    @Test
    public void testLiveQuoteOfTheDayService() throws Exception {

        RakenEmail email = new RakenEmail();

        SendMailToSendGrid sendGridAPI = new SendMailToSendGridService();
        sendGridAPI.sendMailToSendGrid(email, false);

        assertEquals(true, "No Exception means success");

    }
}
