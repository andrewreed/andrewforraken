# Andrew's technical evaluation for Raken #

## Running this ##
I've used Maven as my build tool, so to do a **maven package springboot:run** will start the server up on port 8080.
The service is accessible by doing a post request to localhost:8080/v1/rakenapi/send.  

Or using curl  
`curl --location --request POST 'localhost:8080/v1/rakenapi/send' \
 --header 'Content-Type: application/json' \
 --header 'Authorization: Bearer 23232323232' \
 --data-raw '{
 	"to": "andrew.b.reed@gmail.com",
 	"body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus risus erat, fermentum a enim ut, aliquam accumsan orci. Curabitur et lacus ut mi consectetur lacinia nec id orci. Vestibulum et vulputate ex, vel convallis turpis. Pellentesque semper tempor ex, sed volutpat ante imperdiet maximus. Vivamus eu ipsum orci. Maecenas tellus mi, elementum quis ullamcorper sed, consequat non metus. Fusce ac mi in mauris luctus lacinia. Phasellus eu dictum nisl, pellentesque aliquam tellus. Aenean venenatis ac elit in rhoncus. Curabitur volutpat faucibus dolor id dapibus. Donec a laoreet leo. Nam vel rhoncus est, nec condimentum ipsum. Sed sagittis, tellus ut ullamcorper finibus, mi arcu viverra lorem, nec gravida magna nisl id felis. Sed non ante non mauris fermentum iaculis vitae nec neque.",
 	"from": "me@example.com",
 	"subject": "This is a great test"
 }'`


Because This is a Restful API, I've decided to indicate success with the HTTP Status code of 200. Failures will be either 400 or 500 series, usually accompanied with a string of text to help describe what has failed. 

## List of improvements ##
* I feel sick by leaving an API Key in the clear in the properties file on a public repository. Ideally I'd change that to be selected via a configuration service, or passed as an argument to the runtime, and not stored in a public repository. Even a private repository, where "production" keys should be managed separately to dev/test keys should be seperate. 
* My Error handling from the SendGrid api is somewhat basic. And I have, for now, taken their status 202 and converted to 200. The 202 would be more appropriate for me to return as well
* Normally I would have preferred to include a Swagger endpoint to have illustrated how to use the API. But I've concentrated on the code for this :-)
* Testing. My code coverage isn't complete. 
* I'm not entirely happy with the JsonIgnoreProperties ignoring unknown properties. Because if one of the required fields isn't supplied then we are looking at my clumsy validation logic. In a more complex example trying to use @JsonProperty(required) might be better
* I've not bothered with Spring Boot Actuator or other Management API's. But having a health check end point etc would be valuable for a "production service"


### As per the following brief ###

`_Raken Email Service
Task:
Implement a Spring Boot REST API which allows sending an email to one or more recipients.

Email delivery should be done via sendgrid.com. You can signup for a free trial account for your testing here.

You can generate a starter Spring Boot application from start.spring.io.

Please implement service in Java 8 and use whichever build tool (Gradle / Maven) you feel most comfortable with.

Email API input:

- To - Required
- CC / BCC - Optional 
- Body - Text/HTML 
- Subject
Output:

Whatever you think is appropriate for this REST API
Additional features

Implement API tests and appropriate test coverage for this API.
Add a config toggle which if enabled will filter out and log emails targeted to non rakenapp.com email domains.
Add an optional boolean QueryParam enrich=true to enrich the email messages by appending a random quote of the day, the current Weather in Carlsbad (Raken HQ) or any other piece of data which you can pull from an external REST API as they pass through this service.
Please consider that you are shipping this service into prod and feel free to add any additional functionality that you think is neccessary but please do not spend any longer then a couple of hours on this task. If you like please include a prioritised list of improvements you would make in a following sprint. The goal here is to understand how you approach designing and implementing REST APIs and get a feel for your coding & problem solving skills not to provide completely polished code. Please feel free to submit a partial solution if neccessary or reach out for clarification on anything.

Please add your project to a git repository in either bitbucket or github and send a link to patrick@rakenapp.com when you are happy with your solution._`


